package com.hbs.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hbs.entity.EmployeeDetails;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @PostMapping
    public ResponseEntity<?> storeEmployeeDetails(@Validated @RequestBody EmployeeDetails employeeDetails) {
        // Implement storing employee details logic, handle validation errors
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/tax-deduction")
    public ResponseEntity<?> calculateTaxDeduction() {
        // Implement tax deduction calculation logic
        return ResponseEntity.ok().build();
    }
}
