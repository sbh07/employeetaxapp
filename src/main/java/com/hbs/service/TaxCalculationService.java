package com.hbs.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

@Service
public class TaxCalculationService {
	public BigDecimal calculateTax(BigDecimal yearlySalary) {
		BigDecimal taxAmount = BigDecimal.ZERO;
		if (yearlySalary.compareTo(BigDecimal.valueOf(250000)) > 0
				&& yearlySalary.compareTo(BigDecimal.valueOf(500000)) <= 0) {
			taxAmount = yearlySalary.subtract(BigDecimal.valueOf(250000)).multiply(BigDecimal.valueOf(0.05));
		} else if (yearlySalary.compareTo(BigDecimal.valueOf(500000)) > 0
				&& yearlySalary.compareTo(BigDecimal.valueOf(1000000)) <= 0) {
			taxAmount = BigDecimal.valueOf(12500)
					.add(yearlySalary.subtract(BigDecimal.valueOf(500000)).multiply(BigDecimal.valueOf(0.10)));
		} else if (yearlySalary.compareTo(BigDecimal.valueOf(1000000)) > 0) {
			taxAmount = BigDecimal.valueOf(62500)
					.add(yearlySalary.subtract(BigDecimal.valueOf(1000000)).multiply(BigDecimal.valueOf(0.20)));
		}
		return taxAmount;
	}
	
	public BigDecimal calculateCess(BigDecimal yearlySalary) {
        if (yearlySalary.compareTo(BigDecimal.valueOf(2500000)) > 0) {
            return yearlySalary.subtract(BigDecimal.valueOf(2500000)).multiply(BigDecimal.valueOf(0.02));
        }
        return BigDecimal.ZERO;
    }
}
