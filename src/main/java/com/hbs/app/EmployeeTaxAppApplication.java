package com.hbs.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeTaxAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeTaxAppApplication.class, args);
	}
}